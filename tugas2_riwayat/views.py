from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from tugas2_profile.models import *
import requests

# Create your views here.
response = {}
RIWAYAT_API = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def index(request, npm):
	if not 'user_login' in request.session.keys():
		return HttpResponseRedirect(reverse('status:login'))
	elif Mahasiswa.objects.filter(kode_identitas=npm).exists():
		mahasiswa = Mahasiswa.objects.get(kode_identitas=request.session['kode_identitas'])
		set_data_for_session(request, npm)
		response['riwayats'] = get_riwayat()
		html = 'tugas2_riwayat/riwayat.html'
		# TODO ngambil rahasia dari objek mahasiswa
		response['rahasia'] = not mahasiswa.tampilkan_nilai
		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('status:dashboard', args=(request.session['kode_identitas'],)))


def get_riwayat():
	riwayat = requests.get(RIWAYAT_API)
	return riwayat.json()

def set_data_for_session(request, npm):
    response['kode_identitas'] = request.session['kode_identitas']

    mahasiswa = Mahasiswa.objects.get(kode_identitas=npm)
    response['nama'] = mahasiswa.nama

    status = Status.objects.filter(mahasiswa=mahasiswa)
    response['count_status'] =  status.count()
    response['last_status'] = status.last()
    response['kode_identitas_visit'] = npm
    response['app_id'] = 3
