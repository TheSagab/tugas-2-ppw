from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from tugas2_profile.models import *

# Create your views here.
response={}
def index(request, npm):
    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('status:login'))
    elif Mahasiswa.objects.filter(kode_identitas=npm).exists():
        response["mahasiswas"]=Mahasiswa.objects.all()
        set_data_for_session(request, npm)
        return render(request, 'tugas2_search.html',response)
    else:
        return HttpResponseRedirect(reverse('status:dashboard', args=(request.session['kode_identitas'],)))

def set_data_for_session(request, npm):
    response['kode_identitas'] = request.session['kode_identitas']

    mahasiswa = Mahasiswa.objects.get(kode_identitas=npm)
    response['nama'] = mahasiswa.nama

    status = Status.objects.filter(mahasiswa=mahasiswa)
    response['count_status'] =  status.count()
    response['last_status'] = status.last()
    response['kode_identitas_visit'] = npm
    response['app_id'] = 4