from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from tugas2_profile.models import *

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class SearchUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        
    def test_search_is_exist(self):
        response = Client().get('/search/1606884413/')
        self.assertEqual(response.status_code,302)

    def test_search_not_login(self):
        response = self.client.get('/search/1606884413/')
        self.assertEqual(response.status_code,302)
        self.assertRedirects(response,'/status/login/',302,200)

    def test_login_session_search(self):
        mahasiswa=Mahasiswa()
        mahasiswa.kode_identitas='1606884413'
        mahasiswa.save()
        session = self.client.session
        session['user_login'] = 'wikan.setiaji'
        session['kode_identitas'] = '1606884413'
        session.save()
        response = self.client.get('/search/1606884413/')
        self.assertEqual(response.status_code,200)

    def test_search_person_after_login(self):
        response = self.client.post('/login/auth_login/', {'username': self.username, 'password': self.password})

        response = self.client.get('/search/1606822642/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/search/1234567891/')
        self.assertEqual(response.status_code, 302)