# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse

from tugas2_profile.models import Mahasiswa, Status
from .forms import Status_Form

response = {}

def index(request):
    if 'user_login' in request.session:
        response['login'] = True
        return HttpResponseRedirect(reverse('status:dashboard', args=(request.session['kode_identitas'],)))
    else:
        response['login'] = False
        html = 'home.html'
        return render(request, html, response)

def login(request):
    html = 'login.html'
    return render(request, html, response)

def dashboard(request, npm):
    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('status:login'))
    elif Mahasiswa.objects.filter(kode_identitas=npm).exists():
        response['can_submit'] = False
        if (npm == request.session['kode_identitas']):
            response['can_submit'] = True
        mahasiswa = Mahasiswa.objects.get(kode_identitas=npm)
        status = Status.objects.filter(mahasiswa=mahasiswa)

        response['status'] = status
        response['status_form'] = Status_Form
        set_data_for_session(request, npm)
        html = 'status/status.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect(reverse('status:dashboard', args=(request.session['kode_identitas'],)))

def set_data_for_session(request, npm):
    response['kode_identitas'] = request.session['kode_identitas']

    mahasiswa = Mahasiswa.objects.get(kode_identitas=npm)
    response['nama'] = mahasiswa.nama

    status = Status.objects.filter(mahasiswa=mahasiswa)
    response['count_status'] =  status.count()
    response['last_status'] = status.last()
    response['kode_identitas_visit'] = npm
    response['app_id'] = 1

def add_status(request):
    #print("add_status")
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        text = request.POST['text']
        mahasiswa = Mahasiswa.objects.get(kode_identitas=request.session['kode_identitas'])

        status = Status(mahasiswa=mahasiswa, text=request.POST['text'])
        status.save()
    return HttpResponseRedirect(reverse('status:dashboard', args=(request.session['kode_identitas'],)))