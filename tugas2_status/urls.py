from django.conf.urls import url
from .views import index, login, dashboard, add_status
from .custom_auth import auth_login, auth_logout

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^login/$', login, name='login'),
	url(r'^add_status', add_status, name='add_status'),
    url(r'^(?P<npm>.*)/$', dashboard, name='dashboard'),
    
]

