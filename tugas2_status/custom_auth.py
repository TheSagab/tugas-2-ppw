from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from .csui_helper import get_access_token, verify_user
from tugas2_profile.models import Mahasiswa, Status
#authentication
def auth_login(request):
    print ("#==> auth_login ")
    kode_identitas = 0
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        #call csui_helper
        access_token = get_access_token(username, password)
        if access_token is not None:
            ver_user = verify_user(access_token)
            kode_identitas = ver_user['identity_number']
            role = ver_user['role']

            if not Mahasiswa.objects.filter(kode_identitas=kode_identitas).exists():
                mahasiswa = Mahasiswa.objects.create(kode_identitas=kode_identitas)
                mahasiswa.save()

            # set session
            request.session['user_login'] = username
            request.session['access_token'] = access_token
            request.session['kode_identitas'] = kode_identitas
            request.session['role'] = role
            messages.success(request, "Anda berhasil login")
        else:
            messages.error(request, "Username atau password salah")
    return HttpResponseRedirect(reverse('status:dashboard', args=(kode_identitas,)))

def auth_logout(request):
    print ("#==> auth logout")
    request.session.flush() # menghapus semua session
    return HttpResponseRedirect(reverse('status:index'))
