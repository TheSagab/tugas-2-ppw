from django.test import TestCase
from django.test import Client
from django.urls import resolve

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class StatusUnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_status_url_is_exist(self):
		response = self.client.get('/status/')
		self.assertEqual(response.status_code, 200)

	def test_login_failed(self):
		response = self.client.post('/login/auth_login/', {'username': 'siapa', 'password': 'saya'})
		response = self.client.get('/status/')
		html_response = response.content.decode('utf-8')
		self.assertIn('Login', html_response)

	def test_login_success(self):
		response = self.client.post('/login/auth_login/', {'username': self.username, 'password': self.password})
		response = self.client.get('/status/')
		self.assertEqual(response.status_code, 302)

	def test_index_page_when_logged_in_and_logged_out(self):
		response = self.client.get('/status/')
		self.assertTemplateUsed(response, 'home.html')

		response = self.client.post('/login/auth_login/', {'username': self.username, 'password': self.password})
		response = self.client.get('/status/')
		self.assertEqual(response.status_code, 302)

	def test_logout(self):
		response = self.client.post('/login/auth_login/', {'username': self.username, 'password': self.password})

		response = self.client.get('/login/auth_logout')
		self.assertEqual(response.status_code, 302)

	def test_search_person_after_login(self):
		response = self.client.post('/login/auth_login/', {'username': self.username, 'password': self.password})

		response = self.client.get('/status/1606822642/')
		self.assertEqual(response.status_code, 200)

		response = self.client.get('/status/1234567891/')
		self.assertEqual(response.status_code, 302)

	def test_add_status(self):
		response = self.client.post('/login/auth_login/', {'username': self.username, 'password': self.password})
		response = self.client.post('/status/add_status/', {'text': 'Testing Tugas PPW 2'})
		response = self.client.get('/status/')
		html_response = response.content.decode('utf-8')
		self.assertIn(html_response, 'Testing Tugas PPW 2')

	def test_search_npm_and_login_page_without_login(self):
		response = self.client.get('/status/1234567890/')
		self.assertEqual(response.status_code, 302)

		response = self.client.get('/status/login/')
		self.assertEqual(response.status_code, 200)