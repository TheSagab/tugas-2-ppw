from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Status tidak boleh kosong.',
        'invalid': 'Status terlalu panjang.'
    }
    text_attrs = {
        'type': 'text',
        'cols': 40,
        'rows': 3,
        'class': 'status-text',
        'placeholder':'Update status anda'
    }

    text = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=text_attrs), error_messages=error_messages)
