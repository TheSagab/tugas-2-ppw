"""tugas2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import tugas2_profile.urls as profile
import tugas2_riwayat.urls as riwayat
import tugas2_search.urls as search
from tugas2_status.custom_auth import auth_login, auth_logout
import tugas2_status.urls as status

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url = '/status/', permanent = False), name='index'),
    url(r'^profile/', include(profile,namespace='profile')),
    url(r'^riwayat/', include(riwayat,namespace='riwayat')),
    url(r'^search/', include(search,namespace='search')),
    url(r'^status/', include(status,namespace='status')),
    url(r'^login/auth_login', auth_login, name='auth_login'),
    url(r'^login/auth_logout', auth_logout, name='auth_logout'),
]
