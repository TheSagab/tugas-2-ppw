from django.forms import ModelForm
from .models import *
from django import forms

class Mahasiswa_Form(forms.Form):
    error_messages = {
        'required': 'Field tidak boleh kosong.',
        'invalid': 'Field terlalu panjang.'
    }
    text_attrs = {
        'type': 'text',
        'cols': 40,
        'rows': 3,
        'class': 'status-text',
        'placeholder':'Update status anda'
    }

    keahlian = forms.ModelMultipleChoiceField(queryset=CodeKnowledge.objects.all(),)

class Keahlian_Form(forms.Form):
    error_messages = {
        'required': 'Field tidak boleh kosong.',
        'invalid': 'Field terlalu panjang.'
    }
    text_attrs = {
        'type': 'text',
        'cols': 40,
        'rows': 3,
        'class': 'status-text',
        'placeholder':'Update status anda'
    }

    bahasa = forms.ChoiceField(choices=LANGUAGE_CHOICES, label='', error_messages=error_messages)
    level  = forms.ChoiceField(choices=LEVEL_CHOICES, label=' Level ',  error_messages=error_messages)



