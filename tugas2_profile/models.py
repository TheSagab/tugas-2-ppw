from django.db import models

LANGUAGE_CHOICES = (
    ('none','-'),
    ('js', 'JavaScript'),
    ('java', 'Java'),
    ('css', 'CSS'),
    ('py','Python'),
    ('php', 'PHP'),
    ('rb', 'Ruby'),
    ('cpp', 'C++'),
    ('c', 'C'),
    ('sh', 'Shell'),
    ('cs', 'C#')
)
LEVEL_CHOICES = (
    ('-', '-'),
    ('0', 'Beginner'),
    ('1', 'Intermediate'),
    ('2', 'Advanced'),
    ('3', 'Expert'),
    ('4', 'Legend')
)
# Create your models here.
class CodeKnowledge(models.Model) :

    bahasa      = models.CharField(choices=LANGUAGE_CHOICES,max_length=4, default='none')
    level       = models.CharField(choices=LEVEL_CHOICES,max_length=1, default='none')

    def __str__(self):
        if self.bahasa == 'none':
            return "Kosong"

        return self.bahasa + " level " + self.level

class Mahasiswa(models.Model) :
    kode_identitas  = models.CharField('Kode Identitas', max_length=10, primary_key=True, )
    keahlian        = models.ManyToManyField(CodeKnowledge)
    linkedin        = models.CharField(default="Kosong",max_length=254)
    nama            = models.CharField(default="Kosong",max_length=254)
    email           = models.CharField(default="Kosong",max_length=254)
    tampilkan_nilai = models.BooleanField(default=True)

    def __str__(self):
        return self.kode_identitas

class Status(models.Model):
    mahasiswa    = models.ForeignKey(Mahasiswa)
    text         = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
