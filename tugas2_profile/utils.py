from .models import Mahasiswa, CodeKnowledge

def check_keahlian_in_database(request, kode_keahlian):
    is_exist = False
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    count_keahlian = MovieKu.objects.filter(pengguna=pengguna, kode_keahlian=kode_keahlian).count()
    if count_keahlian > 0 :
        is_exist = True

    return is_exist

def check_keahlian_in_session(request, kode_keahlian):
    is_exist = False
    ssn_key = request.session.keys()
    if 'movies' in ssn_key:
        movies = request.session['movies']
        if kode_keahlian in movies:
            is_exist = True

    return is_exist

def add_item_to_database(request, id):
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    movieku = MovieKu()
    movieku.kode_keahlian = id
    movieku.pengguna = pengguna
    movieku.save()

def add_item_to_session(request, id):
    ssn_key = request.session.keys()
    if not 'movies' in ssn_key:
        request.session['movies'] = [id]
    else:
        movies = request.session['movies']
        # check apakah di session sudah ada key yang sama
        if id not in movies:
            movies.append(id)
            request.session['movies'] = movies

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna

def get_parameter_request(request):
    if request.GET.get("judul"):
        judul = request.GET.get("judul")
    else:
        judul = "-"

    if request.GET.get("tahun"):
        tahun = request.GET.get("tahun")
    else:
        tahun = "-"

    return judul, tahun

# after login, save movies from session
def save_keahlians_to_database(pengguna, list_keahlian_id):
    #looping get id, cek apakah exist berdasarkan user, jika tidak ada, maka tambah

    for movie_id in list_keahlian_id:
        if not (MovieKu.objects.filter(pengguna = pengguna, kode_keahlian = movie_id).count()) > 0:
            new_keahlian = MovieKu()
            new_keahlian.pengguna = pengguna
            new_keahlian.kode_keahlian = movie_id
            new_keahlian.save()

#return movies user from db
def get_my_keahlians_from_database(request):
    resp = []
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    items = MovieKu.objects.filter(pengguna=pengguna)
    for item in items:
        resp.append(item.kode_keahlian)
    return resp

#get my movies from session
def get_my_keahlians_from_session(request):
    resp = []
    ssn_key = request.session.keys()
    if 'movies' in ssn_key:
        resp = request.session['movies']
    return resp

#get detail list movie from api
def get_list_keahlian_from_api(my_list):
    print ("GET LIST DATA")
    list_keahlian = []
    for movie in my_list:
        list_keahlian.append(get_detail_keahlian(movie))

    return list_keahlian