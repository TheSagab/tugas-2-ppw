# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-13 07:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tugas2_profile', '0006_remove_mahasiswa_tampilkan_nilai'),
    ]

    operations = [
        migrations.AddField(
            model_name='mahasiswa',
            name='tampilkan_nilai',
            field=models.BooleanField(default=True),
        ),
    ]
