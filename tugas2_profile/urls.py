from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^edit/$', to_edit, name='to_edit'),
    url(r'^edits/$', edit_mahasiswa, name='edits'),
    url(r'^keahlian/$', to_keahlian, name='to_keahlian'),
    url(r'^keahlians/$', add_keahlian, name='keahlians'),
    url(r'^(?P<npm>.*)/$', index, name='index'),
]
