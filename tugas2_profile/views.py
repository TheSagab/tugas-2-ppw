from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .models import *
from .forms import Mahasiswa_Form, Keahlian_Form
# Create your views here.

response = {}
def index(request, npm):
    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('status:login'))
    elif Mahasiswa.objects.filter(kode_identitas=npm).exists():
        mahasiswa = Mahasiswa.objects.get(kode_identitas=npm)
        response['mahasiswa'] = mahasiswa
        response['mahasiswa.keahlian'] = mahasiswa.keahlian.all()
        response['nama'] = mahasiswa.nama
        response['kode_identitas'] = request.session['kode_identitas']
        status = Status.objects.filter(mahasiswa=mahasiswa)
        response['count_status'] = status.count()
        response['last_status'] = status.last()
        set_data_for_session(request, npm)
        return render(request, 'tugas2_profile.html', response)
    else:
        return HttpResponseRedirect(reverse('status:dashboard', args=(request.session['kode_identitas'],)))

def to_edit(request):
    response['form'] = Mahasiswa_Form
    return render(request, 'tugas2_edit.html', response)

def to_keahlian(request):
    response['form'] = Keahlian_Form
    return render(request, 'tugas2_keahlian.html', response)

def edit_mahasiswa(request):
    form = Mahasiswa_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        keahlian = request.POST['keahlian']
        mahasiswa = Mahasiswa.objects.get(kode_identitas=request.session['kode_identitas'])

        mahasiswa.keahlian.add(keahlian)
        mahasiswa.save()
    return HttpResponseRedirect(reverse('status:dashboard', args=(request.session['kode_identitas'],)))

def add_keahlian(request):
    form = Keahlian_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        bahasa = request.POST['bahasa']
        level = request.POST['level']
        try:
            if CodeKnowledge.objects.get(bahasa=bahasa).count() > 0:
                return HttpResponseRedirect(reverse('profile:to_edit'))
            else:
                keahlian = CodeKnowledge.objects.get(bahasa=bahasa)
                keahlian.level = level
                keahlian.save(update_fields=["level"])
                return HttpResponseRedirect(reverse('profile:to_edit'))
        except:
            keahlian = CodeKnowledge.objects.create(bahasa=bahasa, level=level)
            keahlian.save()
    return HttpResponseRedirect(reverse('profile:to_edit'))



    # if not 'user_login' in request.session.keys():
    #     return HttpResponseRedirect(reverse('status:login'))
    # elif Mahasiswa.objects.filter(kode_identitas=npm).exists():
    #     response['can_submit'] = False
    #     if (npm == request.session['kode_identitas']):
    #         response['can_submit'] = True
    #     mahasiswa = get_object_or_404(Mahasiswa, kode_identitas=npm)
    #
    # if request.method == 'POST':
    #     form = Mahasiswa_Form(request.POST, instance=mahasiswa)
    #     if form.is_valid():
    #         if 'keahlian' in form.cleaned_data:
    #             mahasiswa.keahlian = form.cleaned_data['keahlian']
    #
    #         mahasiswa.save()
    # else:
    #     defaults = { }
    #     if mahasiswa.keahlian:
    #         defaults['keahlian'] = [t.pk for t in mahasiswa.keahlian.all()]
    #
    #     form = mahasiswaForm(request.POST, instance=mahasiswa)
    #
    # variables = RequestContext(request, {'form': form})
    # return render_to_response('tugas2_edit.html', variables)

def set_data_for_session(request, npm):
    response['kode_identitas'] = request.session['kode_identitas']

    mahasiswa = Mahasiswa.objects.get(kode_identitas=npm)
    response['nama'] = mahasiswa.nama

    status = Status.objects.filter(mahasiswa=mahasiswa)
    response['count_status'] =  status.count()
    response['last_status'] = status.last()
    response['kode_identitas_visit'] = npm
    response['app_id'] = 2