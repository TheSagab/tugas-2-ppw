## Anggota Kelompok:
1. Anindito Bhagawanta (1606879230)
   Fitur yang dikerjakan: Riwayat
2. Fany Azka Hidayatulloh (1606888790)
   Fitur yang dikerjakan: Profile
3. Kautsar Fadlillah (1606822642)
   Fitur yang dikerjakan: Status
4. Wikan Setiaji (1606884413)
   Fitur yang dikerjakan: Search

Fitur yang dikerjakan bersama: base.html, dashboard, css

[Link herokuapp](tujuh-dua.herokuapp.com)

Pipeline status: 
[![pipeline status](https://gitlab.com/TheSagab/tugas-2-ppw/badges/master/pipeline.svg)](https://gitlab.com/TheSagab/tugas-2-ppw/commits/master)

Coverage report: 
[![coverage report](https://gitlab.com/TheSagab/tugas-2-ppw/badges/master/coverage.svg)](https://gitlab.com/TheSagab/tugas-2-ppw/commits/master)
